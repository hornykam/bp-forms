<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 11/8/2018
 * Time: 10:49 AM
 */

namespace App\data\managers;

use App\data\entity\FormSummary;
use App\data\entity\Question;
use Nette\Utils\DateTime;

class FormManager
{
    public $em;

    public function __construct(\Kdyby\Doctrine\EntityManager $em)
    {
        $this->em = $em;
    }

    public function deleteForm($id)
    {
        $form = $this->em->find(FormSummary::class, $id);
        if ($form) {
            $this->em->remove($form);
            $this->em->flush();
            return true;
        }
        return false;
    }

    public function createNewForm($form_name, $basic_info, $created_by)
    {
        $form = new FormSummary();
        $form->insertValues($form_name, $basic_info, new DateTime(), $created_by, null);
        $this->em->persist($form);
        $this->em->flush();
        $id = $form->getId();
        return $id;
    }

    public function getFormInfo()
    {
        $forms = $this->em->getRepository(FormSummary::class);

        $form = $forms->findAll();
        $id = end($form)->getId();
        $name = end($form)->getName();
        $basic_info = end($form)->getBasicInfo();
        return array('id' => $id, 'form_name' => $name, 'basic_info' => $basic_info);
    }

    public function getFormsName($form_id)
    {
        $form = $this->em->getRepository(FormSummary::class)->findOneBy(array('id' => $form_id));
        return $form->getName();
    }

    public function getFormSummary($form_id)
    {
        $form = $this->em->getRepository(FormSummary::class)->findOneBy(array('id' => $form_id));
        return $form;
    }

    public function getFormsBasicInfo($form_id)
    {
        $form = $this->em->getRepository(FormSummary::class)->findOneBy(array('id' => $form_id));
        return $form->getBasicInfo();
    }


    public function getAllForms()
    {
        $forms = $this->em->getRepository(FormSummary::class)->findAll();
        return $forms;
    }

    public function getFormQuestions($form_id)
    {
        $questions = $this->em->getRepository(Question::class);
        $form_questions = $questions->findBy(array('form_id' => $form_id));
        return $form_questions;
    }

    public function getQuestionsValues($form_id)
    {
        $questionTypes = ['TextField', 'TextArea', 'Checkbox', 'Integer', 'RadioList', 'CheckboxList']; //, 'Multiselect'
        $questionNames = ['Textové pole', 'Odstavec', 'Zaškrtávací pole', 'Celé číslo', 'Výběr jedné odpovědi', 'Výběr více odpovědí']; //, 'Multiselect'

        $questions = $this->em->getRepository(Question::class);
        $form_questions = $questions->findBy(array('form_id' => $form_id));
        $question_values = array();
        foreach ($form_questions as $question) {
            $values = $question->getValues();
            $values[0]['form_id'] = $form_id;
            $index = array_search($values[0]['type'], $questionTypes);
            $values[0]['type'] = $questionNames[$index];
            array_push($question_values, $values[0]);
        }
        return $this->sortQuestions($question_values);;
    }

    private function sortQuestions($question_values)
    {
        $sorted_values = array();
        $ranking_arr = array();
        foreach ($question_values as $item) array_push($ranking_arr, $item['ranking']);
        sort($ranking_arr);
        for ($i = 0; $i < sizeof($question_values); $i++) {
            foreach ($question_values as $item) {
                if ($ranking_arr[$i] == $item['ranking']) {
                    $sorted_values[$i] = $item;
                    continue;
                }
            }
            $sorted_values[$i]['order'] = ($i + 1);
        }
        return $sorted_values;
    }


    public function getOneQuestion($id)
    {
        $questions = $this->em->getRepository(Question::class);
        $form_questions = $questions->findOneBy(array('id' => $id));
        return $form_questions;
    }

    public function createQuestion($form_id, $question, $type, $answers, $otherAnswer, $required, $max_len, $min_len)
    {
        $new_question = new Question();
        $questions = $this->em->getRepository(Question::class);
        $form_questions = $questions->findBy(array('form_id' => $form_id));
        $max_ranking = 1;
        foreach ($form_questions as $item) {
            if ($item->getRanking() >= $max_ranking) $max_ranking = $item->getRanking() + 1;
        }
        $new_question->insertValues($form_id, $question, $type, $answers, $otherAnswer, $required, $max_len, $min_len, $max_ranking);
        $this->em->persist($new_question);
        $this->em->flush();
    }

    public function deleteQuestion($id)
    {
        $question = $this->em->find(Question::class, $id);
        if ($question) {
            $this->em->remove($question);
            $this->em->flush();
            return true;
        }
        return false;
    }

    public function updateQusetion($form_id, $id, $question, $questionType, $answers, $otherAnswer, $required, $maxLen, $minLen)
    {
        $updated_question = $this->em->find(Question::class, $id);
        $updated_question->updateValues($form_id, $id, $question, $questionType, $answers, $otherAnswer, $required, $maxLen, $minLen);
        $this->em->persist($updated_question);
        $this->em->flush();
    }

    public function getLastFormId()
    {
        $forms = $this->em->getRepository(FormSummary::class)->findAll();
        return max($forms)->getId();
    }

    public function moveQuestionDown($form_id, $id)
    {
        $questions = $this->em->getRepository(Question::class);
        $question = $questions->findOneBy(array('id' => $id));
        $question_ranking = $question->getRanking();
        if ($question_ranking > 999) return;
        $questions = $this->em->getRepository(Question::class);
        $form_questions = $questions->findBy(array('form_id' => $form_id));
        $tmp_ranking = PHP_INT_MAX;
        $tmp_id = $id;
        foreach ($form_questions as $item) {
            if ($question_ranking < $item->getRanking() && $item->getRanking() < $tmp_ranking) /// ????
            {
                $tmp_ranking = $item->getRanking();
                $tmp_id = $item->getId();
            }
        }
        $question1 = $questions->findOneBy(array('id' => $id));
        $question1->setRanking($tmp_ranking);
        $question2 = $questions->findOneBy(array('id' => $tmp_id));
        $question2->setRanking($question_ranking);
        $this->em->persist($question1);
        $this->em->flush();

    }

    public function moveQuestionUp($form_id, $id)
    {
        $questions = $this->em->getRepository(Question::class);
        $question = $questions->findOneBy(array('id' => $id));
        $question_ranking = $question->getRanking();
        if ($question_ranking <= 1) return;
        $questions = $this->em->getRepository(Question::class);
        $form_questions = $questions->findBy(array('form_id' => $form_id));
        $tmp_ranking = 0;
        $tmp_id = 0;
        foreach ($form_questions as $item) {
            if ($question_ranking > $item->getRanking() && $item->getRanking() > $tmp_ranking) {
                $tmp_ranking = $item->getRanking();
                $tmp_id = $item->getId();
            }
        }

        $question1 = $questions->findOneBy(array('id' => $id));
        $question1->setRanking($tmp_ranking);
        $question2 = $questions->findOneBy(array('id' => $tmp_id));
        $question2->setRanking($question_ranking);

        $this->em->persist($question1);
        $this->em->flush();

    }

    public function updateBasicInfo($form_id, $new_name, $new_basic_info, $user)
    {
        $form = $this->em->getRepository(FormSummary::class)->findOneBy(array('id' => $form_id));
        $form->setName($new_name);
        $form->setBasicInfo($new_basic_info);
        $form->setModified($user);
        $this->em->persist($form);
        $this->em->flush();
    }

    public function setModified($form_id, $modified_by)
    {
        $form = $this->em->getRepository(FormSummary::class)->findOneBy(array('id' => $form_id));
        $form->setModified($modified_by);
        $this->em->persist($form);
        $this->em->flush();
    }

    public function copyFormQuestions($selected_form_id, $edited_form_id)
    {
        $new_questions = $this->em->getRepository(Question::class)->findBy(array('form_id' => $selected_form_id));
        $old_questions = $this->em->getRepository(Question::class)->findBy(array('form_id' => $edited_form_id));
        $ranking = [0];
        $ids = [0];
        foreach ($old_questions as $item) {
            array_push($ranking, $item->getRanking());
            array_push($ids, $item->getId());
        }
        $ranking = max($ranking);
        $id = max($ids);
        foreach ($new_questions as $item) {
            $new_question = new Question();
            $new_question->setValues(++$id, $edited_form_id, $item->getQuestion(), $item->getType(), $item->getAnswers(), $item->getMaxLen(), $item->getMinLen(), $item->getRequired(), $item->getOtherAnswer(), ++$ranking);
            $this->em->persist($new_question);
        }
        $this->em->flush();
    }
}
