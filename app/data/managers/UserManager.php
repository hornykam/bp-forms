<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 11/7/2018
 * Time: 11:51 AM
 */

namespace App\data\managers;

use App\data\entity\User;

class UserManager
{
    public $em;

    public function __construct(\Kdyby\Doctrine\EntityManager $em)
    {
        $this->em = $em;
    }

    public function getUserId($username)
    {
        $users = $this->em->getRepository(User::class);
        $user = $users->findOneBy(array('username' => $username));
        if ($user == Null)
        {
            return Null;
        }
        return $user->getId();
    }

    public function getUserCertification($id)
    {
        if(is_null($id)) return null;
        $users = $this->em->getRepository(User::class);
        $user = $users->findOneBy(array('id' => $id));
        return $user->getCertification();
    }

    public function getUsersCorrectPassword($username)
    {
        $user = $this->em->getRepository(User::class)->findOneBy(array('username' => $username));
        if (is_null($user)) return null;
        return $user->getPassword();
    }

    public function getUsersPassword($id)
    {
        $user = $this->em->getRepository(User::class)->findOneBy(array('id' => $id));
        if (is_null($user)) return null;
        return $user->getPassword();
    }

    public function getUserName($id)
    {
        $users = $this->em->getRepository(User::class);
        $user = $users->findOneBy(array('id' => $id));
        return $user->getUsername();
    }

    public function createUser($username, $passowrd, $cetification)
    {
        $user = new User();
        $user->setValues($username, $passowrd, $cetification);
        $this->em->persist($user);
        $this->em->flush();
    }

    public function deleteUser($id)
    {
        $form = $this->em->find(User::class, $id);
        if ($form) {
            $this->em->remove($form);
            $this->em->flush();
            return true;
        }
        return false;
    }

    public function getAllUsers()
    {
        $users =  $this->em->getRepository(User::class)->findAll();
        return $users;
    }

    public function changeUserPassword($id, $new_password)
    {
        $user =  $this->em->getRepository(User::class)->findOneBy(array('id' => $id));
        $user->setPassword($new_password);
        $this->em->persist($user);
        $this->em->flush();
    }
}