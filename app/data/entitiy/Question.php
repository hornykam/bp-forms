<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/24/2018
 * Time: 10:50 PM
 */

namespace App\data\entity;

use Nette\Application\UI\Form;
use Nette\Utils\Json;

/**
 * @ORM\Entity
 * @ORM\Table(name="questions")
 */
class Question
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $form_id;

    /**
     * @ORM\Column(type="string")
     */
    protected $question;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $answers;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $max_len;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $min_len;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $required;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $other_answer;

    /**
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $ranking;

    public function insertValues($form_id, $question, $type, $answers, $otherAnswer, $required, $max_len, $min_len, $ranking)
    {
        $this->form_id = $form_id;
        $this->ranking = $ranking;
        $this->question = $question;
        $this->type = $type;
        $this->answers = $answers;
        $this->required = $required;
        $this->max_len = $max_len;
        $this->min_len = $min_len;
        $this->other_answer = $otherAnswer;
    }

    public function updateValues($form_id, $id, $question, $questionType, $answers, $otherAnswer, $required, $maxLen, $minLen)
    {
        $this->form_id = $form_id;
        $this->id = $id;
        $this->question = $question;
        $this->type = $questionType;
        $this->answers = $answers;
        $this->required = $required;
        $this->max_len = $maxLen;
        $this->min_len = $minLen;
        $this->other_answer = $otherAnswer;
    }

    public function getValues()
    {
        if ($this->answers != array()) {
            $answers = implode(", ", Json::decode($this->answers, Json::FORCE_ARRAY));
        } else {
            $answers = "";
        }
        return array(['id' => $this->id, 'form_id' => $this->form_id, 'question' => $this->question, 'type' => $this->type, //            'answers' => implode(", ", Json::decode($this->answers, Json::FORCE_ARRAY)),
            'answers' => $answers, 'max_len' => $this->max_len, 'min_len' => $this->min_len, 'required' => $this->required, 'ranking' => $this->ranking]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getRanking()
    {
        return $this->ranking;
    }

    public function setRanking($ranking)
    {
        $this->ranking = $ranking;
    }

    public function setQuestion()
    {
//        $this->question = 'asss';
    }

    public function getType()
    {
        return $this->type;
    }

    public function getQuestion()
    {
        return $this->question;
    }

    public function getMaxLen()
    {
        return $this->max_len;
    }

    public function getMinLen()
    {
        return $this->min_len;
    }

    public function getRequired()
    {
        return $this->required;
    }

    public function getDecodedAnswers()
    {
        if ($this->answers != array()) {
            $this->answers = Json::decode($this->answers);
        }
        return $this->answers;
    }

    public function getAnswers()
    {
        return $this->answers;
    }


    public function renderQuestion(Form $form)
    {
        if ($this->answers != array()) {
            $this->answers = Json::decode($this->answers);
        }
//        bdump($form);

        switch ($this->type) {
            case 'TextField':
                $form->addText($this->id,$this->ranking . '. ' . $this->question)
                    ->setRequired($this->required)
                    ->addRule(Form::MIN_LENGTH, 'Text muze mit minimalne %d znaku', $this->min_len)
                    ->addRule(Form::MAX_LENGTH, 'Text muze mit maximalne %d znaku', $this->max_len);
                break;
            case 'TextArea':
                $form->addTextArea($this->id, $this->ranking . '. ' .  $this->question)
                    ->setRequired($this->required)
                    ->addRule(Form::MIN_LENGTH, 'Text muze mit minimalne %d znaku', $this->min_len)
                    ->addRule(Form::MAX_LENGTH, 'Text muze mit maximalne %d znaku', $this->max_len);
                break;
            case 'Integer':
                $form->addInteger($this->id, $this->ranking . '. ' .  $this->question)
                    ->setRequired($this->required)
                ->addRule($form::RANGE, 'Číslo musí být v rozsahu mezi %d a %d.', [$this->min_len, $this->max_len]);
//                              ->addRule(Form::MIN_LENGTH, 'Číslo musí být menší než %d!', $item->min_len)
//                              ->addRule(Form::MAX_LENGTH, 'Číslo musí být větší než %d!', $item->max_len);
                break;
            case 'Checkbox':
                $form->addCheckbox($this->id,$this->ranking . '. ' .  $this->question)->setDefaultValue($this->other_answer)->setRequired($this->required);
                break;
            case 'CheckboxList':
//                $answers = $this->getAnswers($this->answers);
                $form->addCheckboxList($this->id, $this->ranking . '. ' .  $this->question, $this->answers)->setRequired($this->required);
                if ($this->other_answer == true) $form->addCheckbox('o'.$this->id, 'Jiné: ');
                break;
            case 'RadioList':
//                $form->addRadioList($this->id, $this->question, $this->answers)->setRequired($this->required);
//                if ($this->other_answer == true) $form->addCheckbox('o'.$this->id, 'Jiné: ');
                $answers = $this->answers;
                if ($this->other_answer) {
                    $answers = array_merge($answers , ['Jiné']); //prepise klice od 0/1
                }
                $form->addRadioList($this->id, $this->ranking . '. ' .  $this->question, $answers)->setRequired($this->required);
                if ($this->other_answer) {
                    $form->addText('o'.$this->id, 'Jiné: ');
                }

                break;
            case 'Multiselect':
                $form->addMultiselect($this->id, $this->ranking . '. ' .  $this->question, $this->answers)->setRequired($this->required);
                if ($this->other_answer == true) $form->addCheckbox('o'.$this->id, 'Jiné: ');
                break;
        }
//        bdump($form);
        return $form;
    }

    public function setValues($id, $form_id, $question, $type, $answers, $max_len, $min_len, $required, $other_answer, $ranking)
    {
        $this->id = $id;
        $this->form_id = $form_id;
        $this->question = $question;
        $this->type = $type;
        $this->answers = $answers;
        $this->max_len = $max_len;
        $this->min_len = $min_len;
        $this->required = $required;
        $this->other_answer = $other_answer;
        $this->ranking = $ranking;
    }

    /**
     * @return mixed
     */
    public function getFormId()
    {
        return $this->form_id;
    }

    /**
     * @return mixed
     */
    public function getOtherAnswer()
    {
        return $this->other_answer;
    }



}