<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/24/2018
 * Time: 11:09 PM
 */

namespace App\data\entity;

/**
 * @ORM\Entity
 * @ORM\Table(name="answers")
 */
class Answers
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $asigned_form_id;

//    /**
//     * @ORM\Column(type="integer")
//     */
//    protected $order;

    /**
     * @ORM\Column(type="string")
     */
    protected $question;

    /**
     * @ORM\Column(type="string")
     */
    protected $answer;
}