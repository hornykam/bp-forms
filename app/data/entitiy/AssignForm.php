<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/24/2018
 * Time: 11:06 PM
 */

namespace App\data\entity;

/**
 * @ORM\Entity
 */
class AssignForm
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $form_id;

    /**
     * @ORM\Column(type="string")
     */
    protected $target_user;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $answerd;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

}