<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/31/2018
 * Time: 3:24 PM
 */

namespace App\data\entity;

/**
 * @ORM\Entity
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="string")
     */
    protected $certification;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $registrated;

    public function getId()
    {
        return $this->id;
    }


    public function getCertification()
    {
        return $this->certification;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRegistrated()
    {
        if (is_null($this->registrated)) return null;
        return $this->registrated->format('d. m. Y');
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setValues($username, $password, $certification)
    {
        $this->username = $username;
        $this->password = $password;
        $this->certification = $certification;
        $this->registrated = new \DateTime();


    }
}