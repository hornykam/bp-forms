<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/24/2018
 * Time: 9:58 PM
 */

namespace App\data\entity;

/**
 * @ORM\Entity
 * @ORM\Table(name="forms_summary")
 */
class FormSummary
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    // unique=true
    /**
     * @ORM\Column(type="string", length=190)
     */
    protected $form_name;

    /**
     * @ORM\Column(type="text", length=65535, nullable=true)
     */
    protected $basic_info;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $crated_at;

    /**
     * @ORM\Column(type="string")
     */
    protected $creator;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $modified_by;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $modified_at;

    public function insertValues($form_name, $basic_info, $created_at, $created_by, $modified_at)
    {
        $this->form_name = $form_name;
        $this->basic_info = $basic_info;
        $this->crated_at = $created_at;
        $this->creator = $created_by;
        $this->modified_at = $modified_at;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->form_name;
    }

    public function getBasicInfo()
    {
        return $this->basic_info;
    }

    public function getCreatedAt()
    {
        return $this->crated_at->format('d. m. Y H:i');
    }

    public function getModifiedAt()
    {
        if (is_null($this->modified_at)) return null;
        return $this->modified_at->format('d. m. Y H:i');
    }

    public function setName($name)
    {
        $this->form_name = $name;
    }

    public function setBasicInfo($basic_info)
    {
        $this->basic_info = $basic_info;
    }

    public function setModified($user)
    {
        $this->modified_by = $user;
        $this->modified_at = new \DateTime();
    }

    public function getCreator()
    {
        return $this->creator;
    }

    public function getModifiedBy()
    {
        return $this->modified_by;
    }


}