<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/24/2018
 * Time: 6:41 PM
 */

namespace App\factories;

use App\data\entity\Pokus;
use App\data\entity\Test;
use Nette;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;

//use App\data\entity\User;

class SignFactory
{
//    private $id;
//    private $username;
//    private $certification;

    /** @var Nette\Http\Session */
    private $session;

    /** @var Nette\Http\SessionSection */
    private $sessionSection;

    /**
     * @var \Kdyby\Doctrine\EntityManager
     */
    public $EntityManager;

    private $um;

    public function __construct(\App\data\managers\UserManager $um, Nette\Http\Session $session)
    {
        $this->um = $um;

        $this->session = $session;
        $this->sessionSection = $session->getSection('user');
    }

    public function create()
    {
        $form = new Form;
        $form->addText('name', 'Jméno:');    //->setRequired('Zadejte prosím jméno');
        $form->addPassword('password', 'Heslo:');
        $form->addSubmit('login', 'Prihlasit');
        $form->onSuccess[] = [$this, 'signFormSuceeded'];
        return $form;
    }

    public function signFormSuceeded($form, $values)
    {
        if ($this->checkSign($values->name, $values->password)) {
            return True;
        } else return false;
    }


    /**
     * Checks if inserted data are correct.
     */
    private function checkSign($username, $checkedPassword)
    {
//        $this->insertUser('a', 'a', 'admin');
        $realPassword = $this->um->getUsersCorrectPassword($username);
//        $password = Passwords::hash($checkedPassword, ['cost' => 12]);
//        bdump($password);
//        bdump($realPassword);
        bdump(passwords::verify($checkedPassword, $realPassword));
        if (Passwords::verify($checkedPassword, $realPassword)) {

            $id = $this->um->getUserId($username);
            $checkedUsename = $this->um->getUserName($id);
            if ($checkedUsename === $username) {
                $this->sessionSection->id = $id;
                $this->sessionSection->name = $username;
                $this->sessionSection->certification = $this->um->getUserCertification($id);
                $this->sessionSection->setExpiration(0, 'certification');
            }
        }
    }

//    private function insertUser($username, $password)
//    {
//        $password = Passwords::hash($password, ['cost' => 12]);
//        bdump($password);
//        $this->um->createUser($username, $password, 'pacient');
//    }

}