<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/17/2018
 * Time: 7:07 PM
 */

namespace App\factories;

use Nette;
use Nextras;


class BasicInfoFactory
{
    private $fm;
    private $form_id;

    public function __construct(\App\data\managers\FormManager $fm)
    {
        $this->fm = $fm;
    }

    public function create($form_id, $user)
    {
        $form = new Nette\Application\UI\Form();
        $form->addText('form_name', 'Název: ')->setRequired('Je potřeba vyplnit název formuláře!');
        $form->addTextarea('basic_info', 'Popis: ');
        if ($form_id != null) {
            $edited_form = $this->fm->getFormSummary($form_id);
//            bdump($edited_form->getName());
            $form->setDefaults(['form_name' => $edited_form->getName()]);
            bdump($edited_form->getBasicInfo());
            $form->setDefaults(['basic_info' => $edited_form->getBasicInfo()]);
        }
        $form->addSubmit('save_basic_info', 'Ulozit zakladni informace');

        $form->onSuccess[] = function ($form, $values) use ($form_id, $user) {
            if (is_null($form_id)) {
                $this->form_id = $this->fm->createNewForm($values->form_name, $values->basic_info, $user);
            } else {
                $this->form_id = $this->fm->updateBasicInfo($form_id, $values->form_name, $values->basic_info, $user);
            }

        };

        return $form;
    }

    public function getFormId()
    {
        return $this->form_id;
    }

}