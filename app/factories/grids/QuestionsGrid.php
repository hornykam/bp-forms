<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/17/2018
 * Time: 9:07 PM
 */

namespace App\factories\grids;

use Nette\Utils\Paginator;
use Nextras;

class QuestionsGrid
{
    private $fm;

    private $form_id;

    public function __construct(\App\data\managers\FormManager $fm)
    {
        $this->fm = $fm;
    }

    /**
     * @param $form_id
     * @return Nextras\Datagrid\Datagrid
     */
    public function create($form_id)
    {
        $this->form_id = $form_id;
        $grid = new Nextras\Datagrid\Datagrid();
        $grid->addColumn('ranking', 'Poradi');
        $grid->addColumn('id', 'Id');
        $grid->addColumn('question', 'Otazka');
        $grid->addColumn('type', 'Typ');
        $grid->addColumn('answers', 'Odpovedi');
        $grid->addColumn(' ', ' ');
        $grid->addColumn('form_id', 'form_id');
        $grid->addColumn('show2', 'Akce');
        $grid->addCellsTemplate(__DIR__ . './gridTemplates/@QusetionGrid.latte');

        $grid->setDataSourceCallback(function ($filter, $order, Paginator $paginator = NULL) use ($form_id) {
            $data = $this->fm->getQuestionsValues($form_id);
//            bdump($data);
            $itemsPerPage = $paginator->getItemsPerPage();
            $offset = $paginator->getOffset();

            if ($paginator) {
                return array_slice($data, $offset, $itemsPerPage);
            } else {
                return $data;
            }
        });

        $grid->setPagination(15, function () use ($form_id) {
            $data = $this->fm->getQuestionsValues($form_id);
            return sizeof($data);
        });

        return $grid;
    }
}