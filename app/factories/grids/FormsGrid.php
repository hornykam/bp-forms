<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/17/2018
 * Time: 12:28 AM
 */

namespace App\factories\grids;

use Nette\Utils\Paginator;
use Nextras;

class FormsGrid
{
    private $fm;

    public function __construct(\App\data\managers\FormManager $fm)
    {
        $this->fm = $fm;
    }


    public function createBasicFormGrid()
    {
        $grid = new Nextras\Datagrid\Datagrid();
        $grid->addColumn('a', '#');
        $grid->addColumn('form_name', 'Jmeno formulare');
        $grid->addColumn('basic_info', 'Popis');
        $grid->addColumn('creator', 'Vytvoril');
        $grid->addColumn('crated_at', 'Vytvoreno');
        $grid->addColumn('modified_at', 'Upraveno');
        $grid->addColumn('modified_by', 'Upraveno');
        $grid->addColumn('showx', 'Akce');

        $grid->addCellsTemplate(__DIR__ . './gridTemplates/@FormGrid.latte');


        $grid->setDatasourceCallback(function ($filter, $order, Paginator $paginator = NULL) {
            $forms = $this->fm->getAllForms();
            $data = array();
            foreach ($forms as $order => $item) {
                $row = array('a' => $order + 1 . '. ', 'basic_info' => $item->getBasicInfo(), 'form_name' => $item->getName(), 'form_id' => $item->getId(), 'modified_at' => $item->getModifiedAt(), 'created_at' => $item->getCreatedAt(), 'creator' => $item->getcreator(), 'modified_by' => $item->getModifiedBy(), 'showx');
                array_push($data, $row);
            }
            $itemsPerPage = $paginator->getItemsPerPage();
            $offset = $paginator->getOffset();
            if ($paginator) {
                return array_slice($data, $offset, $itemsPerPage);
                return $data;
            } else {
                return $data;
            }
        });

        $grid->setPagination(15, function () {
            $forms = $this->fm->getAllForms();
            return sizeof($forms);
        });

//        $grid->setFilterFormFactory(function() {
//            $form = new Container();
//            $form->addText('a');
//            $form->addText('form_name');
//            $form->addText('creator');
//            $form->addText('crated_at');
//            $form->addText('modified_at');
//            $form->addText('modified_by');
//            return $form;
//        });

        return $grid;
    }



    public function createSimpleFormGrid($edited_form_id)
    {

        $grid = new Nextras\Datagrid\Datagrid();
        $grid->addColumn('a', '#');
        $grid->addColumn('edited_form_id', 'Id');
        $grid->addColumn('form_name', 'Jmeno formulare');
        $grid->addColumn('basic_info', 'Popis');
        $grid->addColumn('modified_at', 'Upraveno');
        $grid->addColumn('modified_by', 'Upraveno');
        $grid->addColumn('crated_at', 'Vytvoreno');
        $grid->addColumn('creator', 'Vytvoril');
        $grid->addColumn('action', 'Akce');

        $grid->addCellsTemplate(__DIR__ . './gridTemplates/@CopyFormGrid.latte');

        $grid->setDatasourceCallback(function ($filter, $order, Paginator $paginator = NULL) use ($edited_form_id) {
            $forms = $this->fm->getAllForms();
            $data = array();
            foreach ($forms as $order => $item) {
                $row = array('a' => $order + 1 . '. ', 'basic_info' => $item->getBasicInfo(), 'form_name' => $item->getName(), 'edited_form_id' => $edited_form_id, 'form_id' => $item->getId(), 'modified_at' => $item->getModifiedAt(), 'created_at' => $item->getCreatedAt(), 'creator' => $item->getcreator(), 'modified_by' => $item->getModifiedBy(), 'showx');
                array_push($data, $row);
            }

            $itemsPerPage = $paginator->getItemsPerPage();
            $offset = $paginator->getOffset();
            if ($paginator) {
                return array_slice($data, $offset, $itemsPerPage);
                return $data;
            } else {
                return $data;
            }

        });

        $grid->setPagination(15, function () {
            $forms = $this->fm->getAllForms();
            return sizeof($forms);
        });

        return $grid;
    }
}