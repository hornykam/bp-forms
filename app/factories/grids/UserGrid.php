<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 12/22/2018
 * Time: 10:36 AM
 */

namespace App\factories\grids;

use Nette\Utils\Paginator;
use Nextras;


class UserGrid
{
    private $um;

    public function __construct(\App\data\managers\UserManager $um)
    {
        $this->um = $um;
    }

    public function create()
    {
        $grid = new Nextras\Datagrid\Datagrid();
        $grid->addColumn('id');
        $grid->addColumn('#', '#');
        $grid->addColumn('username', 'Jméno uživatele');
        $grid->addColumn('certification', 'Oprávnění');
        $grid->addColumn('registrated', 'Registrován');
        $grid->addColumn('action', 'Action');
        $grid->addCellsTemplate(__DIR__ . './gridTemplates/@UserGrid.latte');

        $grid->setDataSourceCallback(function ($filter, $order, Paginator $paginator = NULL) {
            $data = array();
            $users = $this->um->getAllUsers();
            foreach ($users as $order => $item) {
                $row = array('id' => $item->getId(), '#' => $order + 1, 'username' => $item->getUsername(),
                    'certification' => $item->getCertification(), 'registred' => $item->getRegistrated());
                array_push($data, $row);
            }

            $itemsPerPage = $paginator->getItemsPerPage();
            $offset = $paginator->getOffset();

            if ($paginator) {
                return array_slice($data, $offset, $itemsPerPage);
            } else {
                return $data;
            }
        });

        $grid->setPagination(20, function () {
            $data = $this->um->getAllUsers();
            return sizeof($data);
        });

        return $grid;

    }
}