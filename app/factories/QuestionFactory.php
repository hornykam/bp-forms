<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/17/2018
 * Time: 10:41 PM
 */

namespace App\factories;

use Nette;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Nette\Utils\Json;

class QuestionFactory
{
    private $fm;
    private $questionTypes = ['TextField', 'TextArea', 'Checkbox', 'Integer', 'RadioList', 'CheckboxList']; //, 'Multiselect'
    private $questionNames = ['Textové pole', 'Odstavec', 'Zaškrtávací pole', 'Celé číslo', 'Výběr jedné odpovědi', 'Výběr více odpovědí']; //, 'Multiselect'
    private $success;

    private $form;

    /** @var \App\Factories\Grids\FormsGrid @inject */
    public $formsGrid;

    public function __construct(\App\data\managers\FormManager $fm)
    {
        $this->fm = $fm;
    }

    public function create($id, $form_id)
    {
        if (is_null($id)) {
            $type = 'TextField';
            $type = 'Textové pole';
            $question = null;
            $answers = null;
            $required = null;
        }
        else {
            $quest = $this->fm->getOneQuestion($id);
            $type = $quest->getType();
            $type = $this->questionNames[array_search($type, $this->questionTypes)];
            $question = $quest->getQuestion();
            $maxLen = $quest->getMaxLen();
            $minLen = $quest->getMinLen();
            $required = $quest->getRequired();
            $answers = $quest->getDecodedAnswers();
            $otherAnswer = $quest->getOtherAnswer();
        }
        $this->success = False;
        $form = new Nette\Application\UI\Form;
        $form->addSelect('questionType', 'Typ otazky:', $this->questionNames)->setDefaultValue(array_search($type, $this->questionNames));
        $form->addSubmit('select', 'Vybrat')->setValidationScope([$form['questionType']]);
        $form->onValidate[] = [$this, 'showNext'];

        if (!is_null($id)) {
            $this->showNext($form, $answers);
            $this->setDefaultValues($form, $question, $maxLen, $minLen, $required, $otherAnswer);
        }

        $form->onSuccess[] = function (Form $form) use ($form_id, $id, $type, $question, $answers, $required) {
            if (isset($form['save']) && $form->isSubmitted() === $form['save']) {
                $values = $form->getValues($asArray = false);
                if (!isset($values->maxLen)) $values->maxLen = null;
                if (!isset($values->minLen)) $values->minLen = null;
                if (!isset($values->otherAnswer)) $values->otherAnswer = null;
                if ($values->questionType == 4 || $values->questionType == 5 || $values->questionType == 6) {
                    $arr = array();
                    if (!isset($values->answers)) $values->answers = null;
                    foreach ($values->answers as $item) array_push($arr, $item->answer);
                    $answers = Json::encode($arr);
                } else {
                    $answers = Json::encode(array());
                }
                if (is_null($id)) {
                    $this->fm->createQuestion($form_id, $values->question, $this->questionTypes[$values->questionType], $answers, $values->otherAnswer, $values->required, $values->maxLen, $values->minLen);
                } else {
                    $this->fm->updateQusetion($form_id, $id, $values->question, $this->questionTypes[$values->questionType], $answers, $values->otherAnswer, $values->required, $values->maxLen, $values->minLen);
                }
                $this->success = True;
            };
        };
        return $form;
    }

    public function showNext(Form $form, $answers)
    {
        $item = $form['questionType']->getSelectedItem();
        $this->removeUselessComponents($form);
        $form->addText('question', 'Otazka')->setRequired( 'Pole otázka je třeba vyplnit!');
        $form->addSubmit('save', 'Uložit otazku');
        $form->addCheckbox('required', 'Otazka je povina');
        $item = $this->questionTypes[array_search($item, $this->questionNames)];
        switch ($item) {
            case 'TextField':
            case 'TextArea':
                $answers = null;
                $this->textSelected($form);
                break;
            case 'Integer':
                $this->integerSelected($form);
                break;
            case 'Checkbox':
                break;
            case 'CheckboxList':
            case 'RadioList':
            case 'Multiselect':
                $this->multiselectSelected($form, $answers);
                break;
        }
    }

    public function textSelected($form)
    {
        $form->addInteger('maxLen', 'Zadejte maximalni pocet znaku')->setRequired(false);//->setDefaultValue($maxLen);
    }

    public function integerSelected(Form $form)
    {
        $form->addInteger('minLen', 'Minimalni cele cislo: ');
        $form->addInteger('maxLen', 'Maximalni cele cislo: ');
    }

    /**
     * Add container with controlling multiple answers.
     */
    private function multiselectSelected(Form $form, $answ)
    {
        $size = sizeof($answ)>0 ? sizeof($answ) : 1;
        $removeEvent = [$this, 'MyFormRemoveElementClicked'];
        $answers = $form->addDynamic('answers', function (Container $answer) use ($removeEvent, $answ) {
            $value = null;
            if(is_array($answ) && key_exists($answer->name, $answ)) {
                $value = $answ[$answer->name];
            }
            $answer->addText('answer', 'Odpověď: ')->setDefaultValue($value)->setRequired('Nejdříve je potřeba vyplnit všechny odpovědi!');
            $answer->addSubmit('remove', 'Odstranit moznost')->setValidationScope(FALSE)# disables validation
            ->onClick[] = $removeEvent;
        }, $size);
        $answers->addSubmit('add', 'Pridat moznost')->setValidationScope(true)->onClick[] = [$this, 'MyFormAddElementClicked'];
        $form->addCheckbox('otherAnswer', 'Přidat možnost jiné: (Nutno přidat odpověď Jiné)');
    }

    public function MyFormAddElementClicked(Nette\Forms\Controls\SubmitButton $button)
    {
        $button->parent->createOne();
    }

    public function MyFormRemoveElementClicked(Nette\Forms\Controls\SubmitButton $button)
    {
        $form = $button->parent->parent;
        $form->remove($button->parent, TRUE);
    }

    public function isSaved()
    {
        return $this->success;
    }


    private function setDefaultValues(Form $form, $question, $maxLen, $minLen, $required, $otherAnswer)
    {
        $form['question']->setDefaultValue($question);
        $form['required']->setDefaultValue($required);
        $item = $form['questionType']->getSelectedItem();
        switch ($item) {
            case 'TextField':
                $form['maxLen']->setDefaultValue($maxLen);
                break;
            case 'TextArea':
                $form['maxLen']->setDefaultValue($maxLen);
                break;
            case 'Integer':
                $form['maxLen']->setDefaultValue($maxLen);
                $form['minLen']->setDefaultValue($minLen);
                break;
            case 'Checkbox':
                $form['required']->setDefaultValue($required);
                break;
            case 'CheckboxList':
            case 'RadioList':
            case 'Multiselect':
                $form['otherAnswer']->setValue($otherAnswer);
                break;
        }
        return $this->form;
    }


    /**
     * Remove all containers before creating new ones.
     */
    private function removeUselessComponents($form)
    {
        if (isset($form['question'])) $form->removeComponent($form['question']);
        if (isset($form['minLen'])) $form->removeComponent($form['minLen']);
        if (isset($form['maxLen'])) $form->removeComponent($form['maxLen']);
        if (isset($form['save'])) $form->removeComponent($form['save']);
        if (isset($form['required'])) $form->removeComponent($form['required']);
        if (isset($form['answers'])) $form->removeComponent($form['answers']);
        if (isset($form['otherAnswer'])) $form->removeComponent($form['otherAnswer']);
//        if (isset($answers['add'])) bdump('neco');
    }


}