<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 12/22/2018
 * Time: 7:03 PM
 */

namespace App\factories;

use App\data\entity\Pokus;
use App\data\entity\Test;
use Nette\Application\UI\Form;
use Nette\Security\Passwords;

//use App\data\entity\User;

class ResetPasswordFactory
{
    /**
     * @var \Kdyby\Doctrine\EntityManager
     */
    public $EntityManager;

    private $um;

    private $oldPasswordCorrect = false;
    private $newPasswordsAreSame = false;

    public function __construct(\App\data\managers\UserManager $um)
    {
        $this->um = $um;
    }

    public function create($user_id)
    {
        $this->user_id = $user_id;
//        $user_id = $this->getParameter('user_id');
        $form = new Form();
//        $form->addText('username', 'Uživatelské jméno')->setDefaultValue($this->um->getUserName($user_id))->setOmitted();
        $form->addPassword('old_password', 'Staré heslo');
        $form->addPassword('new_password', 'Nové heslo')->setRequired('Musíte zadat nové heslo!');
        $form->addPassword('password_verification', 'Potvrďte heslo');
        $form->addSubmit('save', 'Ulozit');
        $form->onSuccess[] = [$this, 'changePassword'];
        return $form;
    }

    public function changePassword($form, $values)
    {
            $old_password_hash = $this->um->getUsersPassword($this->user_id);
            if (Passwords::verify($values['old_password'], $old_password_hash)) {
                $this->oldPasswordCorrect = true;
            }
            $hash = Passwords::hash($values['new_password'], ['cost' => 12]);
            if (Passwords::verify($values['password_verification'], $hash)) {
                $this->newPasswordsAreSame = true;
                bdump('zmena hesla');
                    $this->modifyUserPassword($this->user_id, $hash);
//                    $this->flashMessage('Změna hesla byla úspěšná', 'success');
//                    $this->redirect('Profile:userGrid');
            }
    }

    public function getOldPasswordCorrect()
    {
        return $this->oldPasswordCorrect;
    }

    public function sameNewPasswords()
    {
        return $this->newPasswordsAreSame;
    }

    /**
     * Changes user password.
     */
    private function modifyUserPassword($id, $password)
    {
        $this->um->changeUserPassword($id, $password);
    }


}