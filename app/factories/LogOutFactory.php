<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 11/25/2018
 * Time: 6:40 PM
 */

namespace App\factories;

use Nette;
use Nette\Application\UI\Form;

class LogOutFactory
{

    /** @var Nette\Http\Session */
    private $session;

    /** @var Nette\Http\SessionSection */
    private $sessionSection;

    public function __construct(Nette\Http\Session $session)
    {
        $this->session = $session;
        $this->sessionSection = $session->getSection('user');
    }

    public function create()
    {
        $submit = new Form();
        $submit->addSubmit('logout', 'Odhlasit')
            ->onClick[] = function (){
            $this->session->getSection('user')->remove();
        };
        return $submit;
    }
}