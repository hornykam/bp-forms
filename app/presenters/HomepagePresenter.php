<?php

namespace App\Presenters;

use Nette\Application\UI;
use Nette\Application\UI\Form;


class HomepagePresenter extends UI\Presenter
{
    /** @var \App\Factories\SignFactory @inject */
    public $signFactory;

    /**
     * @var \Kdyby\Doctrine\EntityManager
     */
    public $EntityManager;

    private $um;

    public function __construct(\App\data\managers\UserManager $um)
    {
        $this->um = $um;
    }

    protected function createComponentSignForm()
    {
        $signForm = $this->signFactory->create();
        $signForm->onSuccess[] = function (Form $form) {
            $id = $this->session->getSection('user')->id;
            if (is_null($id)) {
                $this->flashMessage('Špatné přihlašovací údaje!', 'error');
                $this->redirect('this');
            }
            if ($id != NULL) {
                $this->flashMessage('Přihlášení bylo úspěšné!', 'success');
                $this->redirect('Profile:default', ['id' => $id]);
            }
        };
        return $signForm;
    }

}