<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/17/2018
 * Time: 8:51 PM
 */

namespace App\Presenters;

use Nette\Application\UI\Form;

class QuestionPresenter extends SecuredPresenter
{
    private $fm;
    private $um;

    /** @var \App\Factories\Grids\FormsGrid @inject */
    public $formsGrid;

    /** @var \App\Factories\Grids\QuestionsGrid @inject */
    public $questionsGrid;

    /** @var \App\Factories\QuestionFactory @inject */
    public $questionFactory;

    /** @var \App\Factories\LogOutFactory @inject */
    public $logout;

    /** @persistent */
    public $form_id;

    private $id;

    public function __construct(\App\data\managers\FormManager $fm, \App\data\managers\UserManager $um)
    {
        $this->fm = $fm;
        $this->um = $um;
    }

    protected function createComponentQuestionsGrid()
    {
        $form_id = $this->getParameter('form_id');
        $grid = $this->questionsGrid->create($form_id);
        return $grid;
    }


    protected function createComponentNewQuestionButton()
    {
        $form = new Form();
        $form->addSubmit('new_question', 'Pridat novou otazku');
        $form_id = $this->getParameter('form_id');
        if (is_null($form_id)) {
            $form_id = $this->fm->getLastFormId();
        }
        $form->onSubmit[] = function() use ($form_id) {
            $this->redirect('Question:new', ['form_id' => $form_id]);
        };
        return $form;
    }

    public function createComponentNewQuestion()
    {
        $form_id = $this->getParameter('form_id');
        if (is_null($form_id)) $form_id = $this->fm->getLastFormId();
        $id = $this->getParameter('id');
        $question = $this->questionFactory->create($id, $form_id);
        $question->onSubmit[] = function (Form $form) use ($form_id) {
            if ($this->questionFactory->isSaved()) $this->redirect(':default', ['form_id' => $form_id]);
        };
        return $question;
    }

    protected function createComponentSaveForm()
    {
        $form = new Form;
        $form->addSubmit('saveForm', 'Hotovo');
        $form->onSuccess[] = function () {
            $this->redirect('Form:');
        };
        return $form;
    }

    public function renderDefault()
    {
        $this->template->form_id = $this->getParameter('form_id');
        $form_id = $this->getParameter('form_id');
        if (is_null($form_id)) $this->redirect('Question:');
        $this->template->form_name = $this->fm->getFormsName($form_id);
        $this->template->basic_info = $this->fm->getFormsBasicInfo($form_id);
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }

    public function actionNew($id, $form_id)
    {

        $form_id = $this->getParameter('form_id');
        $this->template->form_id = $form_id;
        $this->template->form_name = $this->fm->getFormsName($form_id);
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }


    public function handleDelete($form_id, $id)
    {
        if ($this->session->getSection('user')->certification != 'lekar' || $this->session->getSection('user')->certification == 'admin') {
            $this->flashMessage('Na tuto operaci nemate dostatecna prava!', 'error');
            $this->redirect('Homepage:');
        } else {
            $result = $this->fm->deleteQuestion($id);
            if ($result) {
                $this->fm->setModified($form_id, $this->session->getSection('user')->name);
                $this->flashMessage('Otázka byla smazána.', 'success');
            } else {
                $this->flashMessage('Operace neproběhla v pořádku! ', 'error');
            }
            $this->redirect('this', ['form_id' => $form_id]);
        }
    }

    public function actionEdit($form_id, $id)
    {
        $this->template->form_name = $this->fm->getFormsName($form_id);
        $this->template->basic_info = $this->fm->getFormInfo($form_id);
        $this->template->form_id = $form_id;
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }

    public function actionCopy($form_id, $id)
    {
        $this->template->form_name = $this->fm->getFormsName($form_id);
        $this->template->basic_info = $this->fm->getFormInfo($form_id);
        $this->template->form_id = $form_id;
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }

    public function createComponentLogOut()
    {
        $logOutSubmit = $this->logout->create();
        $logOutSubmit->onSuccess[] = function () {
            $this->redirect('Homepage:');
        };
        return $logOutSubmit;
    }

    public function handleMoveUp($form_id, $id)
    {
        $this->fm->setModified($form_id, $this->session->getSection('user')->name);
        $this->fm->moveQuestionUp($form_id, $id);
        $this->redirect('this', ['form_id' => $form_id]);
    }

    public function handleMoveDown($form_id, $id)
    {
        $this->fm->setModified($form_id, $this->session->getSection('user')->name);
        $this->fm->moveQuestionDown($form_id, $id);
        $this->redirect('this', ['form_id' => $form_id]);
    }

    public function createComponentFormGrid()
    {
        $edited_form_id = $this->getParameter('form_id');
        $form_grid = $this->formsGrid->createSimpleFormGrid($edited_form_id);

        return $form_grid;
    }

    public function handleSelect($edited_form_id, $selected_form_id)
    {
//        bdump("vybrany otazky z dotazniku: " . $selected_form_id);
//        bdump("vlozeny otazky do dotazniku: " . $edited_form_id);
        $this->fm->copyFormQuestions($selected_form_id, $edited_form_id);
        $this->redirect(':default', ['form_id' => $edited_form_id]);
    }



}