<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/16/2018
 * Time: 5:11 PM
 */

namespace App\Presenters;

use Nette\Application\UI\Form;
use Nette\Security\Passwords;


class ProfilePresenter extends SecuredPresenter//Nette\Application\UI\Presenter
{
    private $um;
    private $id;

    /** @var \App\Factories\Grids\FormsGrid @inject */
    public $formsGrid;

    /** @var \App\Factories\LogOutFactory @inject */
    public $logout;

    /** @var \App\Factories\Grids\UserGrid @inject */
    public $userGrid;

    /** @var \App\Factories\ResetPasswordFactory @inject */
    public $resetPasswordFactory;

    public function __construct(\App\data\managers\UserManager $um)
    {
        $this->um = $um;
    }

    public function actionDefault()
    {
        $this->id = $this->session->getSection('user')->id;
        $id = $this->session->getSection('user')->id;
        $certification = $this->um->getUserCertification($id);
        switch ($certification) {
            case 'pacient':
                $this->redirect(':patient');
                break;
            case 'lekar':
                $this->redirect(':doctor');
                break;
            case 'admin':
                $this->redirect(':admin');
                break;
        }
    }

    public function renderPatient()
    {
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }

    public function renderDoctor()
    {
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }

    public function renderAdmin()
    {
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }

    public function renderRegister()
    {
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }

    public function actionChangePassword($user_id)
    {
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->changed_password_username = $this->um->getUserName($user_id);
        $this->template->user_id = $user_id;
    }

    public function renderUserGrid()
    {
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }


    public function createComponentFormsubmit()
    {
        $form = new Form;
        $form->addSubmit('red', 'Sprava formularu');
        $form->onSuccess[] = function () {
            $this->redirect('Form:default');
        };
        return $form;
    }


    public function createComponentPatientssubmit()
    {
        $form = new Form();
        $form->addSubmit('red', 'Sprava pacientu');
        return $form;
    }


//    public function actionFill()
//    {
//        $this->redirect('Homepage:');
//    }

    public function createComponentLogOut()
    {
        $logOutSubmit = $this->logout->create();
        $logOutSubmit->onSuccess[] = function () {
            $this->redirect('Homepage:');
        };
        return $logOutSubmit;
    }

    protected function createComponentRegister()
    {
        $form = new Form();
        $form->addText('username', 'Uživatelské jméno');
        $form->addPassword('password', 'Heslo');
        $form->addPassword('password_verification', 'Potvrďte heslo');
        $form->addSubmit('register', 'Registrovat');
        $form->onSuccess[] = function ($form, $values) {
            $user_id = $this->um->getUserId($values['username']);
            if (!is_null($user_id)) {
                $this->flashMessage('Zadané uživatelské jméno už je obsazené!', 'error');
                $this->redirect('this');
            }
            $hash2 = Passwords::hash($values['password_verification'], ['cost' => 12]);
            if (Passwords::verify($values['password'], $hash2)) {
                $this->insertUser($values['username'], $values['password']);
                $this->flashMessage('Registrace byla úspěšná', 'success');
                $this->redirect('Profile:admin');
            } else {
                $this->flashMessage('Zadaná hesla se neshodují', 'error');
            }
        };
        return $form;
    }

    protected function createComponentResetPassword()
    {
        $resetPasswordFactory = $this->resetPasswordFactory->create($this->getParameter('user_id'));
        $resetPasswordFactory->onSubmit[] = function () {
            if (!$this->resetPasswordFactory->getOldPasswordCorrect()) {
                $this->flashMessage('Špatně zadnané staré heslo!', 'error');
            } elseif (!$this->resetPasswordFactory->sameNewPasswords()) {
                $this->flashMessage('Nová hesla se neshodují!', 'error');
            } else {
                $this->flashMessage('Heslo úspěšně změněno.', 'success');
                $this->redirect('Profile:userGrid');
            }

        };
        return $resetPasswordFactory;
    }

    /**
     * Insert new user in database.
     * @param $username
     * @param $password
     */
    private function insertUser($username, $password)
    {
        $password = Passwords::hash($password, ['cost' => 12]);
        $this->um->createUser($username, $password, 'lekar');
    }

    protected function createComponentUserGrid()
    {
        $userGrid = $this->userGrid->create();
        return $userGrid;
    }

    public function handleDelete($id)
    {
        if ($this->session->getSection('user')->id === (int)$id) {
            $this->flashMessage('Tohoto uživatel nemůžete odstranit!', 'error');
            $this->redirect('this');
            return false;
        } elseif ($this->session->getSection('user')->certification !== "admin") {
            $this->flashMessage('Na tuto operaci nemate dostatecna prava!', 'error');
            $this->redirect('Profile:');
        } else {
            $result = $this->um->deleteUser($id);
            if ($result) {
                $this->flashMessage('Uživatel byl úspěšně smazán.', 'success');
            } else {
                $this->flashMessage('Smazání nebylo úspěšné.', 'error');
            }
            $this->redirect('this');
        }
    }

}
