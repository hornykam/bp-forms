<?php
/**
 * Created by PhpStorm.
 * User: horny
 * Date: 10/16/2018
 * Time: 7:28 PM
 */

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;

class FormPresenter extends SecuredPresenter
{

    private $fm;
    private $um;

    /** @var \App\Factories\Grids\FormsGrid @inject */
    public $formsGrid;

    /** @var \App\Factories\Grids\QuestionsGrid @inject */
    public $questionGrid;

    /** @var \App\Factories\BasicInfoFactory @inject */
    public $basciInfoFactory;

    /** @var \App\Factories\LogOutFactory @inject */
    public $logout;

    /** @var persistent */
    private $form_id;

//    public $form;

    public function __construct(\App\data\managers\FormManager $fm, \App\data\managers\UserManager $um)
    {
        $this->fm = $fm;
        $this->um = $um;
    }


    public function actionEdit($form_id)
    {
        $this->template->form_id = $form_id;
    }

    public function handleDelete($id)
    {
        if (($this->session->getSection('user')->certification !== "lekar") && ($this->session->getSection('user')->certification !== "admin")) {
            $this->flashMessage('Na tuto operaci nemate dostatecna prava!', 'error');
            $this->redirect('Homepage:');
        } else {
            $result = $this->fm->deleteForm($id);
            if ($result) {
                $this->flashMessage('Formulář byl úspěšně smazán.', 'success');
            } else {
                $this->flashMessage('Smazání nebylo úspěšné.', 'error');
            }
            $this->redirect('this');
        }
    }

    public function handleEdit($form_id)
    {
        $this->template->form_id = $form_id;
        $this->fm->setModified($form_id, $this->session->getSection('user')->name);
        $this->redirect('Question:default', ['form_id' => $form_id]);
    }


    public function createComponentFormsGrid()
    {
        $forms_grid = $this->formsGrid->createBasicFormGrid();
        return $forms_grid;
    }

    public function createComponentNewForm()
    {
        $form = new Form;
        $form->addSubmit('new_form', 'Novy formular')->onClick[] = [$this, 'createNewForm'];
        return $form;
    }

    public function createNewForm()
    {
        $this->redirect(':new');
    }

    public function createComponentBasicInfo()
    {
        $form_id = $this->getParameter('form_id');
        $user = $this->session->getSection('user')->name;
        $basic_info = $this->basciInfoFactory->create($form_id, $user);
        $basic_info->onSuccess[] = function () use ($form_id) {
            $form_id = $this->getParameter('form_id');
            if (is_null($form_id)) $form_id = $this->fm->getLastFormId();
            $this->redirect('Question:default', ['form_id' => $form_id]);
        };
        return $basic_info;
    }

    public function createComponentQuestions()
    {
        $form_id = $this->getParameter('form_id');
        $form = new Nette\Application\UI\Form;
        $questions = $this->fm->getFormQuestions($form_id);

        $questions = $this->sortQuestions($questions);
        foreach ($questions as $order => $item) {
            $form = $item->renderQuestion($form);
        }
        return $form;
    }

    /**
     * Sorts array of questions according their ranking.
     */
    private function sortQuestions($questions)
    {
        $ranks = array();
        $sorted_questions = array();
        foreach ($questions as $item) array_push($ranks, $item->getRanking());
        sort($ranks);
        for ($i = 0; $i < sizeof($ranks); $i++) {
            foreach ($questions as $item) {
                if ($ranks[$i] == $item->getRanking()) $sorted_questions[$i] = $item;
            }
        }
        return $sorted_questions;
    }

    public function renderDefault()
    {
        $this->template->form_id = $this->getParameter('form_id');
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
    }

    public function renderShow($form_id)
    {
        if (is_null($this->getParameter('form_id'))) $this->redirect('Form:');
        $user_id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($user_id);
        $this->template->user_id = $user_id;
        $this->template->setParameters(['form_name' => $this->fm->getFormsName($form_id), 'basic_info' => $this->fm->getFormsBasicInfo($form_id), 'form_id' => $form_id]);
    }

    public function actionShowToCopy($form_id, $edited_form_id)
    {
        if (is_null($this->getParameter('edited_form_id'))) $this->redirect('Form:');

        $user_id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($user_id);
        $this->template->user_id = $user_id;
        $this->template->edited_form_id = $edited_form_id;
        $this->template->edited_form_name = $this->fm->getFormsName($edited_form_id);
        $this->template->setParameters(['form_name' => $this->fm->getFormsName($form_id), 'basic_info' => $this->fm->getFormsBasicInfo($form_id), 'form_id' => $form_id]);
    }

    public function renderNew($form_id)
    {
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
        $this->template->form_id = $form_id;
//        $this->template->form_id = $this->getParameter('form_id');
    }

    public function renderEdit()
    {
        $form_id = $this->getParameter('form_id');
        if (is_null($form_id)) $this->redirect('Form:');
        $id = $this->session->getSection('user')->id;
        $this->template->username = $this->um->getUserName($id);
        $this->template->user_id = $id;
        $this->template->form_name = $this->fm->getFormsName($form_id);
        $this->template->basic_info = $this->fm->getFormsBasicInfo($form_id);
    }

    public function createComponentLogOut()
    {
        $logOutSubmit = $this->logout->create();
        $logOutSubmit->onSuccess[] = function () {
            $this->redirect('Homepage:');
        };
        return $logOutSubmit;
    }

    public function createComponentCopyQuestions()
    {
        $selected_form_id = $this->getParameter('form_id');
        $edited_form_id = $this->getParameter('edited_form_id');
        $form = new Form;
        $form->addSubmit('select', 'Vybrat dotaznik');
        $form->onSuccess[] = function () use ($selected_form_id, $edited_form_id) {
            $this->fm->copyFormQuestions($selected_form_id, $edited_form_id);
            $this->redirect('Question:default', ['form_id' => $edited_form_id]);
        };

        return $form;

    }


}